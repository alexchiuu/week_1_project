Rails.application.routes.draw do
  resources :users do
    collection do
      get "count"
    end
    member do
      get "address"
    end
  end
  resources :merchants, only: [ :index, :create ] do
    resources :products, only: [ :create, :show ]
    resources :orders, only: [ :index, :new, :create, :show, :update ]
  end
end
