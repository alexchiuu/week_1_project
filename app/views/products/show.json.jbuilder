json.product @product, :title

if @product.product_variations.present?
  json.product_variation @product.product_variations, :styles, :price
else
  json.price @product.price
end