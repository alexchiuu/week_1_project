class OrdersController < ApplicationController
  protect_from_forgery with: :null_session
  before_action :set_order, only: [ :show, :update ]
  
  def index
    @orders = Order.all
    render json: { order: @orders }, status: 200
  end

  def new
    @order = Order.new
  end

  def create
    @order = Order.new(order_params)
    
    if @order.save
      @order.create_orderitems(order_params[:order_items_attributes])
      render json: { order: @order, order_items: @order.order_items, user: @order.user }, status: 201
    else
      render json: { error: @order.errors }, status: 422
    end
  end

  def show
    render json: { order: @order, order_items: @order.order_items }, status: 200
  end

  def update
    if @order.update(status_params)
      render json: { order: @order, user: @order.user }, status: 201
    else
      render json: { error: @order.errors }, status: 422
    end
  end

  private
  def set_order
    @order = Order.find(params[:id])
  end

  def order_params
    params.require(:order).permit(:status,
                                  :customer_name,
                                  :customer_phone,
                                  order_items_attributes: [ :item_id, :item_type, :quantity, :variation_id])
  end

  def status_params
    params.require(:order).permit(:status)
  end
end
