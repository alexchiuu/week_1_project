class ProductsController < ApplicationController
  protect_from_forgery with: :null_session
  before_action :set_merchant, only: [ :create, :show ]
  
  def create
    product = @merchant.products.new(product_params)
    if product.save
      render json: product, status: 201
    else
      render json: product.errors, status: 422
    end
  end

  def show
    @product = @merchant.products.find(params[:id])
  end

  private
  def set_merchant
    @merchant = Merchant.find(params[:merchant_id])
  end

  def product_params
    params.require(:product).permit(:title, 
                                    :quantity, 
                                    :price, 
                                     product_variations_attributes: [ { styles: [] }, :quantity, :price ] )
  end
end