class MerchantsController < ApplicationController
  protect_from_forgery with: :null_session

  def index
    @merchants = Merchant.all
  end

  def create
    merchant = Merchant.new(merchant_params)
    if merchant.save
      render json: merchant, status: 201
    else
      render json: merchant.errors, status: 422
    end
  end

  private
  def merchant_params
    params.require(:merchant).permit(:name, :shop_status)
  end
end