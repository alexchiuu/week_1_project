class Product
  include Mongoid::Document
  include Mongoid::Timestamps
  field :title, type: String
  field :quantity, type: Integer
  field :price, type: Money

  validates :title,
             presence: true
  validates :quantity, 
             numericality: { greater_than_or_equal_to: 0 }

  belongs_to :merchant
  
  embeds_many :product_variations
  accepts_nested_attributes_for :product_variations

  searchkick

  def search_data
    {
      title: title,
      product_variation: product_variations
    }
  end
end
