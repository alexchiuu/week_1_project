class Order
  include Mongoid::Document
  include Mongoid::Timestamps
  field :status, type: String, default: "pending"
  field :customer_name, type: String
  field :customer_phone, type: String
  field :confirmed_at, type: DateTime
  field :completed_at, type: DateTime
  field :cancelled_at, type: DateTime

  validates_inclusion_of :status, in: %w(pending confirmed completed cancelled)

  belongs_to :user, optional: true
  has_many :order_items
  accepts_nested_attributes_for :order_items

  before_update :update_related_status_time, if: :status_changed?
  before_save :establish_relation_with_user
  after_save :update_user_order_count
  
  searchkick
  
  def search_data
    {
      customer_name: customer_name,
      customer_phone: customer_phone
    }
  end
  
  def create_orderitems(order_items_params)
    order_items_params.each do |order_item_params|
      item = Object.const_get(order_item_params[:item_type]).find(order_item_params[:item_id])
      if item.present?
        self.order_items.create(item_id: order_item_params[:item_id],
                                item_type: order_item_params[:item_type], 
                                object_data: item.attributes, 
                                quantity: order_item_params[:quantity],
                                variation_id: order_item_params[:variation_id])
      end
    end
  end
  
  private
  def update_related_status_time
    case self.status
    when "confirmed"
      self.confirmed_at = Time.zone.now
    when "completed"
      self.completed_at = Time.zone.now
    when "cancelled"
      self.cancelled_at = Time.zone.now
    end
  end

  def update_user_order_count
    if user.present? && self.status_changed?
      user.update_order_count
    end
  end

  def establish_relation_with_user
    user = User.find_by(phone: customer_phone)
    if user.present?
      self.user = user
    end 
  end
end
