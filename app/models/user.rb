class User
  include Mongoid::Document
  include Mongoid::Timestamps
  field :name, type: String
  field :phone, type: String
  field :age, type: Integer
  field :address, type: Hash, default: {}
  field :order_count, type: Integer

  validates :name,
             presence: true
  validates :phone, 
             presence: true,
             length: { is: 10 },
             uniqueness: true
  validates :age, 
             numericality: { greater_than: 0 }
  validates :order_count, 
             numericality: { greater_than_or_equal_to: 0 }

  belongs_to :merchant, optional: true
  has_many :orders

  
  def update_order_count
    order_count = self.orders.where(:status.nin => [ "cancelled" ]).count
    self.update(order_count: order_count)
  end
end
