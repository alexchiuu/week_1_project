class Merchant
  include Mongoid::Document
  include Mongoid::Timestamps
  field :name, type: String
  field :shop_status, type: String, default: "open"

  validates :name,
             presence: true
  
  has_many :users
  has_many :products
end
