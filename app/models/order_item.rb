class OrderItem
  include Mongoid::Document
  include Mongoid::Timestamps
  field :item_id, type: BSON::ObjectId
  field :item_type, type: String
  field :object_data, type: Hash, default: {}
  field :quantity, type: Integer
  field :variation_id, type: BSON::ObjectId

  validates :quantity, 
             numericality: { greater_than_or_equal_to: 0 }

  belongs_to :order
end
