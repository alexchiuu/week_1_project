class ProductVariation
  include Mongoid::Document
  include Mongoid::Timestamps
  field :styles, type: Array, default: []
  field :quantity, type: Integer
  field :price, type: Money

  validates :quantity, 
             numericality: { greater_than_or_equal_to: 0 }

  embedded_in :product
end
